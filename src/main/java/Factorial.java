public class Factorial {
    public static void main(String[] args) {
        Factorial factorial = new Factorial();
        System.out.println(factorial.calculate(3));
    }

    public int calculate(int n) {
        if (n == 1) return 1;
        if (n == 0) return 1;
        if (n < 0) return -1;

        return n * calculate(--n);
    }
}
