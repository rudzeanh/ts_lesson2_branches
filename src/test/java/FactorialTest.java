import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class FactorialTest {

    @Test
    public void factorialTest() {
        Factorial factorial = new Factorial();
        int n = 0;
        int expectedValue = 1;

        long result = factorial.calculate(n);

        Assertions.assertEquals(expectedValue, result);
    }
}
